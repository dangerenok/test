module com.dangerenok.mavenproject1 {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.dangerenok.mavenproject1 to javafx.fxml;
    exports com.dangerenok.mavenproject1;
}

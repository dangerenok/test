package com.dangerenok.mavenproject1;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;


public class PrimaryController {
  
    @FXML
    private TableView<Person> table;

    @FXML
    private TableColumn<Person, String> firstNameCol;

    @FXML
    private TableColumn<Person, String> lastNameCol;

    @FXML
    private TableColumn<Person, String> emailCol;

    @FXML
    private TableView<Person> table1;

    @FXML
    private TableColumn<Person, String> firstNameCol1;

    @FXML
    private TableColumn<Person, String> lastNameCol1;

    @FXML
    private TableColumn<Person, String> emailCol1;
    
    @FXML
    private Button btnDang;

    @FXML
    private Button primaryButton;

    @FXML
    void btnDangPush(ActionEvent event) throws IOException {
        Stage stage= new Stage();

        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("secondary.fxml"));
                
        Parent parent=(Parent) fxmlLoader.load();
        Scene scene = new Scene(parent, 640, 480);

        stage.setScene(scene);
        stage.show();
    }
    
   
    @FXML
    private void initialize() {
        
        firstNameCol.setCellValueFactory(
            new PropertyValueFactory<>("firstName"));
        
        firstNameCol.setCellFactory(TextFieldTableCell.<Person>forTableColumn());
        firstNameCol.setOnEditCommit(
            (CellEditEvent<Person, String> t) -> {
                ((Person) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setFirstName(t.getNewValue());
        });
 

        lastNameCol.setCellValueFactory(
            new PropertyValueFactory<>("lastName"));
        lastNameCol.setCellFactory(TextFieldTableCell.<Person>forTableColumn());
        lastNameCol.setOnEditCommit(
            (CellEditEvent<Person, String> t) -> {
                ((Person) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setLastName(t.getNewValue());
        });
 
        emailCol.setCellValueFactory(
            new PropertyValueFactory<>("email"));
        emailCol.setCellFactory(TextFieldTableCell.<Person>forTableColumn());       
        emailCol.setOnEditCommit(
            (CellEditEvent<Person, String> t) -> {
                ((Person) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setEmail(t.getNewValue());
        });
        
        table.setEditable(true);
        table.setItems(Context.getContext().getPersons());


        
        firstNameCol1.setCellValueFactory(
            new PropertyValueFactory<>("firstName"));
        
        firstNameCol1.setCellFactory(TextFieldTableCell.<Person>forTableColumn());
        firstNameCol1.setOnEditCommit(
            (CellEditEvent<Person, String> t) -> {
                ((Person) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setFirstName(t.getNewValue());
        });
 

        lastNameCol1.setCellValueFactory(
            new PropertyValueFactory<>("lastName"));
        lastNameCol1.setCellFactory(TextFieldTableCell.<Person>forTableColumn());
        lastNameCol1.setOnEditCommit(
            (CellEditEvent<Person, String> t) -> {
                ((Person) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setLastName(t.getNewValue());
        });
 
        emailCol1.setCellValueFactory(
            new PropertyValueFactory<>("email"));
        emailCol1.setCellFactory(TextFieldTableCell.<Person>forTableColumn());       
        emailCol1.setOnEditCommit(
            (CellEditEvent<Person, String> t) -> {
                ((Person) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())
                        ).setEmail(t.getNewValue());
        });
        
        table1.setEditable(true);
        table1.setItems(Context.getContext().getPersons());        
    }
    

}
